#Networking and Security in iOS Applications

![ejpt_certificate_sm.png](https://lh6.googleusercontent.com/-uESEAsyyBIQ/AAAAAAAAAAI/AAAAAAAAAAA/gF-pknP_1r0/s0-c-k-no-ns/photo.jpg)

#Link
https://www.coursera.org/learn/security




#Module 1:
Course Overview
#Module 2: 
Social Networking in iOS - Twitter
#Module 3: 
Social Networking in iOS - Facebook
#Module 4:
Social Networking in iOS - UIActivityViewController

Peer-Review - 01 Social Networking App

Assignment: Social Networking App

#Module 5:
Fundamentals of Web API security and data transport
#Module 6:
The new world of iOS secure network settings

Peer Review - 02 Instagram Network API App

Assignment: 02 Instagram Network API App

#Module 7: 
Giving a little pushback: local notifications and actions
#Module 8: 
Checking in from afar: Remote push notifications

Peer Review - 03 Push Notification Alarm

Assignment: 03 Push Notification Alarm


#Module 9: 
Securely Storing Data for Your User in Core Data

Peer Review - 04 Secure Data Storage App

Final Review

#Bonus Module 10: 
Getting your Application out of the gate and into the world

Assignment: 04 Secure Data Storage App

Quiz: Final Exam
