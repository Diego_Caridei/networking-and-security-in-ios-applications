//
//  AppDelegate.m
//  Assignment3
//
//  Created by Diego Caridei on 01/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    application.applicationIconBadgeNumber=0;
    
    UILocalNotification *locaNotif=launchOptions[UIApplicationLaunchOptionsLocalNotificationKey];
    if (locaNotif) {
        UIAlertController *ac= [UIAlertController alertControllerWithTitle:@"Receive on launch" message:locaNotif.alertBody preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action= [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault
                                                      handler:nil];
        [ac addAction:action];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [application.keyWindow.rootViewController presentViewController:ac animated:YES completion:nil];
        });
    }
    
    return YES;
}


-(void)application:(UIApplication *)application handleActionWithIdentifier:(nullable NSString *)identifier forLocalNotification:(nonnull UILocalNotification *)notification completionHandler:(nonnull void (^)())completionHandler{
    
    UIAlertController *ac= [UIAlertController alertControllerWithTitle:@"Receive on action" message:identifier preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action= [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault
                                                  handler:nil];
    [ac addAction:action];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [application.keyWindow.rootViewController presentViewController:ac animated:YES completion:nil];
    });
    
    completionHandler();
    
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    application.applicationIconBadgeNumber=0;
    UIAlertController *ac= [UIAlertController alertControllerWithTitle:@"Receive while running" message:notification.alertBody preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action= [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault
                                                  handler:nil];
    [ac addAction:action];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [application.keyWindow.rootViewController presentViewController:ac animated:YES completion:nil];
    });
}


@end
