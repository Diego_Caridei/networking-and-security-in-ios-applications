//
//  AppDelegate.h
//  ParsePushApp
//
//  Created by Diego Caridei on 01/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

