//
//  AppDelegate.m
//  ParsePushApp
//
//  Created by Diego Caridei on 01/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import "AppDelegate.h"
#import "Parse.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSDictionary *remoteNotification=launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotification) {
        
        NSString *remoteMessage =remoteNotification[@"aps"][@"alert"];
        UIAlertController *ac= [UIAlertController alertControllerWithTitle:@"Receive on launch" message:remoteMessage preferredStyle:UIAlertControllerStyleAlert];
      
        UIAlertAction *action= [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault
                                                      handler:nil];
        [ac addAction:action];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [application.keyWindow.rootViewController presentViewController:ac animated:YES completion:nil];
        });
    }
    
    
    [Parse setApplicationId:@"9Ej4BZRmDQRYg26q0pAqb50JS8Zhwe58XFzlCTUj" clientKey:@"nIl0YCEM54hiC7ufHBev04uOGP4zn6vIS0nHyR9m"];
    
    
    UIUserNotificationType types = UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert;
   UIUserNotificationSettings *settings=[UIUserNotificationSettings settingsForTypes:types categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    return YES;
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    PFInstallation *currentInstallation =[PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo {
    NSString *remoteMessage =userInfo[@"aps"][@"alert"];
    UIAlertController *ac= [UIAlertController alertControllerWithTitle:@"Receive while runnig" message:remoteMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action= [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault
                                                  handler:nil];
    [ac addAction:action];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [application.keyWindow.rootViewController presentViewController:ac animated:YES completion:nil];
    });

}




@end
