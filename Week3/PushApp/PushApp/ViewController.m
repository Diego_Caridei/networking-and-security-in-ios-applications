//
//  ViewController.m
//  PushApp
//
//  Created by Diego Caridei on 01/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
-(void)requestPermissionToNotify;
-(void)createANotification:(int)secondsInTheFuture;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}
- (IBAction)scheduleTapped:(id)sender {
    [self requestPermissionToNotify];
    [self createANotification:5];
}

-(void)requestPermissionToNotify{
    //Definizione delle azionie
    UIMutableUserNotificationAction *floatAction =[[UIMutableUserNotificationAction alloc]init];
    floatAction.identifier = @"FLOAT_ACTION";
    floatAction.title = @"Float";
    floatAction.activationMode = UIUserNotificationActivationModeBackground;
    floatAction.destructive = YES;
    floatAction.authenticationRequired = NO;
    
    
    UIMutableUserNotificationAction *stringAction =[[UIMutableUserNotificationAction alloc]init];
    stringAction.identifier = @"STRING_ACTION";
    stringAction.title = @"String";
    stringAction.activationMode = UIUserNotificationActivationModeBackground;
    stringAction.destructive = NO;
    stringAction.authenticationRequired = NO;
    
    //Definizioni delle categorie
    UIMutableUserNotificationCategory *category =[[UIMutableUserNotificationCategory alloc]init];
    category.identifier = @"MAIN_CATEGORY";
    [category setActions:@[floatAction,stringAction] forContext:UIUserNotificationActionContextDefault];
    
    NSSet *setCategories = [NSSet setWithObjects:category,nil];
    
    
    
    UIUserNotificationType types = UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert;
    UIUserNotificationSettings *settings=[UIUserNotificationSettings settingsForTypes:types categories:setCategories];
    [[UIApplication sharedApplication]registerUserNotificationSettings:settings];
}

-(void)createANotification:(int)secondsInTheFuture{
    
    UILocalNotification *localNotif=[[UILocalNotification alloc]init];
    localNotif.fireDate = [[NSDate date]dateByAddingTimeInterval:secondsInTheFuture];
    localNotif.timeZone=nil;
    localNotif.alertTitle=@"Alert Title";
    localNotif.alertBody=@"Alert Body";
    localNotif.alertAction=@"Okay";
    localNotif.soundName=UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber=10;
    
    localNotif.category=@"MAIN_CATEGORY";
    
    [[UIApplication sharedApplication]scheduleLocalNotification:localNotif];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
