//
//  AppDelegate.h
//  Assignment4
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#include "Person.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
-(Person*)createPerson;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

