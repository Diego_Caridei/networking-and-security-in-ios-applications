//
//  ViewController.h
//  Assignment4
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
#import "AppDelegate.h"
@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *surenameField;
@property (weak, nonatomic) IBOutlet UITextField *mobileFiled;
@property (weak, nonatomic) IBOutlet UILabel *numberOBJ;
@property(nonatomic)AppDelegate *appDelegate;
@property (weak, nonatomic) IBOutlet UILabel *persistedData;
- (IBAction)add:(id)sender;
- (IBAction)deleteOBJ:(id)sender;

@end

