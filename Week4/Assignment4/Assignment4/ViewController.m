//
//  ViewController.m
//  Assignment4
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _appDelegate =[[UIApplication sharedApplication]delegate];
    [self updateLogList];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)add:(id)sender {
    Person *c =[self.appDelegate  createPerson];
    c.nome =self.nameField.text;
    c.cognome =self.surenameField.text;
    c.numero= self.mobileFiled.text;
    [self.appDelegate saveContext];
    [self updateLogList];
    self.nameField.text = @"";
    self.surenameField.text=@"";
    self.mobileFiled.text=@"";
}

- (IBAction)deleteOBJ:(id)sender {
    
    NSManagedObjectContext *moc = self.appDelegate.managedObjectContext;
    NSFetchRequest *request =[NSFetchRequest fetchRequestWithEntityName:@"Person"];
    NSError *error=nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!request) {
        NSLog(@"Error fetching Person objects :%@\n%@",[error localizedDescription],[error userInfo]);
        abort();
    }
    
    for(Person *c in results){
        [moc deleteObject:c];
    }
    [self.appDelegate saveContext];
    [self updateLogList];
    
}

-(void)updateLogList{
    NSManagedObjectContext *moc = self.appDelegate.managedObjectContext;
    NSFetchRequest *request =[NSFetchRequest fetchRequestWithEntityName:@"Person"];
    NSError *error=nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!request) {
        NSLog(@"Error fetching Person objects :%@\n%@",[error localizedDescription],[error userInfo]);
        abort();
    }
    NSMutableString *buffer =[NSMutableString stringWithString:@""];
    int i=0;
    for(Person *c in results){
        i++;
        [buffer appendFormat:@"\n%@ %@ %@",c.nome,c.cognome,c.numero,nil];
    }
    NSLog(@"%@",buffer);
    self.numberOBJ.text=[NSString stringWithFormat:@"%d",i];
    self.persistedData.text= buffer;
}
@end
