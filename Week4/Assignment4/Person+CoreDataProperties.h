//
//  Person+CoreDataProperties.h
//  Assignment4
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Person.h"

NS_ASSUME_NONNULL_BEGIN

@interface Person (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *nome;
@property (nullable, nonatomic, retain) NSString *cognome;
@property (nullable, nonatomic, retain) NSString *numero;

@end

NS_ASSUME_NONNULL_END
