//
//  ChoreLogMO+CoreDataProperties.m
//  CoreData
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ChoreLogMO+CoreDataProperties.h"

@implementation ChoreLogMO (CoreDataProperties)

@dynamic when;
@dynamic chore_done;
@dynamic person_who_did_it;

@end
