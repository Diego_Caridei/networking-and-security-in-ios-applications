//
//  PersonMO.h
//  CoreData
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChoreLogMO;

NS_ASSUME_NONNULL_BEGIN

@interface PersonMO : NSManagedObject

-(NSString *)description;

@end

NS_ASSUME_NONNULL_END

#import "PersonMO+CoreDataProperties.h"
