//
//  PersonMO.m
//  CoreData
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import "PersonMO.h"
#import "ChoreLogMO.h"

@implementation PersonMO


-(NSString *)description{
    return self.person_name;
}

@end
