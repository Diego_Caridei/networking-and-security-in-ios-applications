//
//  PickerViewHelper.h
//  CoreData
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PickerViewHelper : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>

-(void)setArray:(NSArray*)incoming;
-(void)addItemToArray:(NSObject*)thing;
-(NSObject*) getItemFromArray:(NSUInteger)index;

@end
