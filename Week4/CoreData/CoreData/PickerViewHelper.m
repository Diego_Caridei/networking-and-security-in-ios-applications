//
//  PickerViewHelper.m
//  CoreData
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import "PickerViewHelper.h"

@interface PickerViewHelper ()
@property (nonatomic)NSMutableArray *picekrData;
@end

@implementation PickerViewHelper


-(id)init{
    if ([super init]==nil) {
        return nil;
    }
    self.picekrData =[NSMutableArray arrayWithArray:@[]];
    return self;
}

-(void)setArray:(NSArray*)incoming{
    self.picekrData =[NSMutableArray arrayWithArray:incoming];
}
-(void)addItemToArray:(NSObject*)thing{
    [self.picekrData addObject:thing];
}
-(NSObject*) getItemFromArray:(NSUInteger)index{
    return [self.picekrData objectAtIndex:index];
}



-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.picekrData count];
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
   return  [[self.picekrData objectAtIndex:row]description];
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



@end
