//
//  ViewController.h
//  CoreData
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerViewHelper.h"
@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *choreField;
@property (weak, nonatomic) IBOutlet UIPickerView *choreRoller;
@property(nonatomic)PickerViewHelper *choreRollerHelper;
@property (weak, nonatomic) IBOutlet UITextField *personField;
- (IBAction)addPerson:(id)sender;
@property (weak, nonatomic) IBOutlet UIPickerView *personRoller;
@property(nonatomic)PickerViewHelper *personRollerHelper;

@property (weak, nonatomic) IBOutlet UILabel *persistedData;

@end

