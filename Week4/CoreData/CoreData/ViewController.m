//
//  ViewController.m
//  CoreData
//
//  Created by Diego Caridei on 02/11/15.
//  Copyright © 2015 Diego Caridei. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "ChoreMO.h"
#import "PersonMO.h"
@interface ViewController ()

@property(nonatomic)AppDelegate *appDelegate;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _appDelegate =[[UIApplication sharedApplication]delegate];
    
    self.choreRollerHelper = [[PickerViewHelper alloc]init];
    [self.choreRoller setDelegate:_choreRollerHelper];
    [self.choreRoller setDataSource:_choreRollerHelper];
    
    
    self.personRollerHelper = [[PickerViewHelper alloc]init];
    [self.personRoller setDelegate: self.personRollerHelper];
    [self.personRoller setDataSource: self.personRollerHelper];
    
    [self updatePersonRoller];
    [self updateChoreRoller];
    [self updateLogList];
}

- (IBAction)choreTapped:(id)sender {
    ChoreMO *c =[self.appDelegate  createChoreMO];
    c.chore_name =self.choreField.text;
    [self.appDelegate saveContext];
    [self updateLogList];
    [self updateChoreRoller];
}
- (IBAction)deleteTapped:(id)sender {
    NSManagedObjectContext *moc = self.appDelegate.managedObjectContext;
    NSFetchRequest *request =[NSFetchRequest fetchRequestWithEntityName:@"Chore"];
    NSError *error=nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!request) {
        NSLog(@"Error fetching Person objects :%@\n%@",[error localizedDescription],[error userInfo]);
        abort();
    }

    for(ChoreMO *c in results){
        [moc deleteObject:c];
    }
    [self.appDelegate saveContext];
    [self updateLogList];
    

}

-(void)updateLogList{
    NSManagedObjectContext *moc = self.appDelegate.managedObjectContext;
    NSFetchRequest *request =[NSFetchRequest fetchRequestWithEntityName:@"Chore"];
    NSError *error=nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!request) {
        NSLog(@"Error fetching Person objects :%@\n%@",[error localizedDescription],[error userInfo]);
        abort();
    }
    NSMutableString *buffer =[NSMutableString stringWithString:@""];
    for(ChoreMO *c in results){
        [buffer appendFormat:@"\n%@",c.chore_name,nil];
    }

    self.persistedData.text= buffer;
}

-(void)updateChoreRoller{
    NSManagedObjectContext *moc = self.appDelegate.managedObjectContext;
    NSFetchRequest *request =[NSFetchRequest fetchRequestWithEntityName:@"Chore"];
    NSError *error=nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!request) {
        NSLog(@"Error fetching Person objects :%@\n%@",[error localizedDescription],[error userInfo]);
        abort();
    }
    NSMutableArray *choreData =[[NSMutableArray alloc]init];
    for(ChoreMO *c in results){
        [choreData addObject:c.chore_name];
    }
    [_choreRollerHelper setArray:choreData];
    [_choreRoller reloadAllComponents];

}
-(void)updatePersonRoller{
    NSManagedObjectContext *moc = self.appDelegate.managedObjectContext;
    NSFetchRequest *request =[NSFetchRequest fetchRequestWithEntityName:@"Person"];
    NSError *error=nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!request) {
        NSLog(@"Error fetching Person objects :%@\n%@",[error localizedDescription],[error userInfo]);
        abort();
    }
    NSMutableArray *personData =[[NSMutableArray alloc]init];
    for(PersonMO *c in results){
        [personData addObject:c.person_name];
    }
    [_personRollerHelper setArray:personData];
    [_personRoller reloadAllComponents ];
   
}



- (IBAction)addPerson:(id)sender {
    PersonMO *c =[self.appDelegate  createPersonMO];
    c.person_name =self.personField.text;
    [self.appDelegate saveContext];
    [self updatePersonRoller];

}
@end
